package settings

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"sync"

	. "../classes"
)

type connparam struct {
	/* Static content URL */
	StaticURL string `json:"staticurl"`
	/* Static content root folder */
	StaticRoot string `json:"staticroot"`
	HTTPPort   string `json:"httpport"`
	HTTPSPort  string `json:"httpsport"`
	/* Mailer authorization settings */
	SMTPServer   string `json:"smtpserver"`
	SMTPPort     string `json:"smtpport"`
	SMTPLogin    string `json:"smtplogin"`
	SMTPPassword string `json:"smtppassword"`
	/* Registration confirm url */
	ConfirmURL string `json:"confirmurl"`

	mu sync.RWMutex
}

type VSettings struct {
	//raspicam settings
	Width     int    `json:"width"`
	Height    int    `json:"height"`
	Bitrate   int    `json:"bitrate"`
	Framerate int    `json:"framerate"`
	Rotation  int    `json:"rotation"`
	Hflip     bool   `json:"hflip"`
	Vflip     bool   `json:"vflip"`
	Profile   string `json:"profile"` // H264Profile: "baseline", "main", "high"
	Level     string `json:"level"`   ////H264Level "4", "4.1", "4.2"

	//ffmpeg settings
	HlsTime     int `json:"hls_time"`
	HlsListSize int `json:"hls_list_size"`
	HlsWrap     int `json:"hls_wrap"`

	mu sync.RWMutex
}

var (
	par  *connparam
	vpar *VSettings
	once sync.Once
)

func settings() {
	// settings: create a settings instance
	once.Do(func() {
		configFile, err := os.Open("config.json")
		if err != nil {
			fmt.Printf("Opening config file error: %s\n", err.Error())
		}

		jsonParser := json.NewDecoder(configFile)
		if err = jsonParser.Decode(&par); err != nil {
			fmt.Printf("Parsing config file error: %s\n", err.Error())
		}

		vconfigFile, err := os.Open("vconfig.json")
		if err != nil {
			fmt.Printf("Opening config file error: %s\n", err.Error())
		}

		jsonParser = json.NewDecoder(vconfigFile)
		if err = jsonParser.Decode(&vpar); err != nil {
			fmt.Printf("Parsing video config file error: %s\n", err.Error())
		}
	})
}

//GetStaticURL returns a static content URL
func GetStaticURL() string {
	settings()
	par.mu.RLock()
	defer par.mu.RUnlock()
	return par.StaticURL
}

//GetStaticRoot returns a static content root folder
func GetStaticRoot() string {
	settings()
	par.mu.RLock()
	defer par.mu.RUnlock()
	return par.StaticRoot
}

//GetHTTPPort returns a port for http
func GetHTTPPort() string {
	settings()
	par.mu.RLock()
	defer par.mu.RUnlock()
	return par.HTTPPort
}

//GetHTTPSPort returns a port for https
func GetHTTPSPort() string {
	settings()
	par.mu.RLock()
	defer par.mu.RUnlock()
	return par.HTTPSPort
}

//SaveVSettings : store video settings
func SaveVSettings(jsontext []byte) string {
	settings()
	vpar.mu.RLock()
	defer vpar.mu.RUnlock()

	err := ioutil.WriteFile("vconfig.json", jsontext, 0644)
	var vsettings VSettings
	err = json.Unmarshal(jsontext, &vsettings)
	vpar = &vsettings

	result := "success"

	if nil != err {
		result = err.Error()
	}

	return result
}

//GetVSettings : returns video settings
func GetVSettings() string {
	settings()
	vpar.mu.RLock()
	defer vpar.mu.RUnlock()

	result := ""
	res, err := json.Marshal(vpar)

	if nil != err {
		result = err.Error()
	} else {
		result = string(res[:])
	}

	return result
}

func GetRaspividArgs() []string {

	args := []string{
		"-n",
		"-w", strconv.Itoa(vpar.Width),
		"-h", strconv.Itoa(vpar.Height),
		"-fps", strconv.Itoa(vpar.Framerate),
		"-t", "86400000",
		"-b", strconv.Itoa(vpar.Bitrate),
		"-o", "-",
	}

	if vpar.Vflip {
		args = append(args, "-vf")
	}

	if vpar.Hflip {
		args = append(args, "-hf")
	}

	return args
}

func GetFfmpegArgs() []string {
	args := []string{
		"-loglevel", "2",
		"-y",
		"-analyzeduration", "5M",
		"-i", "-",
		"-c:v", "copy",
		"-hls_time", strconv.Itoa(vpar.HlsTime),
		"-hls_list_size", strconv.Itoa(vpar.HlsListSize),
		"-hls_wrap", strconv.Itoa(vpar.HlsWrap),
		"/var/www/html/stream.m3u8",
	}

	return args
}

//GetConfirmURL returns a user confirmation URL
func GetConfirmURL() string {
	settings()
	par.mu.RLock()
	defer par.mu.RUnlock()
	return par.ConfirmURL
}

//GetSender returns a new email sender
func GetSender() Sender {
	settings()
	par.mu.RLock()
	defer par.mu.RUnlock()
	return Sender{par.SMTPLogin, par.SMTPPassword, par.SMTPServer, par.SMTPPort}
}
