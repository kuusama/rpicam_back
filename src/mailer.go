package main

import (
	. "./settings"
)

func Send_mail(addr string, token string) {
	sender := GetSender()

	//The receiver needs to be in slice as the receive supports multiple receiver
	Receiver := []string{addr}

	Subject := "RPiCam registration"
	message := `
	<!DOCTYPE HTML PULBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
	<meta http-equiv="content-type" content="text/html"; charset=UTF-8">
	</head>
	<body>
	Someone want to sign up on RPiCam via your email.<br>
	To confirm registration click the following link:
	<a href=` + GetConfirmURL() + `?token=` + token + `>Confirm registration</a>

	<div><i>
	Regards,<br>
	RPiCam developer.<i></div>
	</body>
	</html>`
	bodyMessage := sender.WriteHTMLEmail(Receiver, Subject, message)

	sender.SendMail(Receiver, Subject, bodyMessage)
}
