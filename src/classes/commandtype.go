package classes

//CommandType is just an int
type CommandType int

//Command type enum
const (
	StartVideo     CommandType = 1
	StopVideo      CommandType = 2
	GetSettings    CommandType = 3
	UpdateSettings CommandType = 4
	Login          CommandType = 5
	AddUser        CommandType = 6
	GetUsers       CommandType = 7
	GetUserData    CommandType = 8
	UpdateUserData CommandType = 9
	DeleteUser     CommandType = 10
)

func (commandtype CommandType) String() string {
	names := [...]string{
		"Unknown",
		"StartVideo",
		"StopVideo",
		"GetSettings",
		"UpdateSettings",
		"Login",
		"AddUser",
		"GetUserData",
		"UpdateUserData",
		"DeleteUser"}

	// → `command`: It's one of the
	// values of Command constants.

	// prevent panicking in case of
	// `command` is out of range of Command
	if commandtype < StartVideo || commandtype > DeleteUser {
		return "Unknown"
	}

	// return the name of a Command
	// constant from the names array
	// above.
	return names[commandtype]
}
