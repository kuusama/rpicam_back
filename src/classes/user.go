package classes

// User class
type User struct {
	Id       int    `json:"id"`
	Login    string `json:"login"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Active   int    `json:"active"`
	Token    string `json:"token"`
	Level    int    `json:"level"`
}
