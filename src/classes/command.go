package classes

// Command class
type Command struct {
	Type    CommandType `json:"type"`
	UserId  int         `json:"userid"`
	Payload string      `json:"payload"`
}
