package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/sevlyar/go-daemon"

	. "./classes"
	. "./settings"
	. "./udatabase"
)

var raspivid, psips, ffmpeg *exec.Cmd
var isRunning bool

func redirect(w http.ResponseWriter, req *http.Request) {
	target := "https://" + req.Host + req.URL.Path

	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	log.Printf("redirect to: %s", target)
	http.Redirect(w, req, target, http.StatusPermanentRedirect)
}

func authUser(user User) string {
	uid := LoginUser(user)
	if "0" == uid && AccountNotActivated(user.Email) {
		uid = "-1"
	}
	return uid
}

func registerUser(user User) string {
	uid := -1 //Email is already registred
	if !CheckEmailRegistred(user.Email) {
		user.Token, uid = RegisterUser(user)
		go Send_mail(user.Email, user.Token)
	}
	return strconv.Itoa(uid)
}

func confirmUser(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	keys, ok := req.URL.Query()["token"]

	if !ok || len(keys[0]) < 1 {
		log.Printf("Url Param 'token' is missing")
		return
	}

	usertoken := keys[0]

	//Redirect to main page
	if ActivateUser(usertoken) {
		http.Redirect(w, req, GetStaticURL(), http.StatusTemporaryRedirect)
	} else {
		http.Error(w, "Invalid token", http.StatusBadRequest)
	}
}

func serveApi(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	req.ParseMultipartForm(2048)
	jsontext := []byte(req.FormValue("command"))
	var command Command
	err := json.Unmarshal(jsontext, &command)
	checkErr(err)
	payload := []byte(command.Payload)

	result := ""
	switch command.Type {
	case StartVideo:
		result = StartVideoStream()

	case StopVideo:
		result = StopVideoStream()

	case GetSettings:
		result = GetVSettings()

	case UpdateSettings:
		result = SaveVSettings(payload)

	case Login:
		result = "-1"
		var user User
		err := json.Unmarshal(payload, &user)
		if nil == err {
			result = authUser(user)
		}

	case AddUser:
		result = "0"
		var user User
		err := json.Unmarshal(payload, &user)
		if nil == err {
			result = registerUser(user)
		}

	case GetUsers:
		jsontext, err := json.Marshal(GetUserList())
		checkErr(err)
		result = bytestostring(jsontext)

	case GetUserData:
		var user User
		err := json.Unmarshal(payload, &user)
		jsontext, err := json.Marshal(GetFullUserData(user))
		checkErr(err)
		result = bytestostring(jsontext)

	case UpdateUserData:
		result = "0"
		var user User
		err := json.Unmarshal(payload, &user)
		if nil == err {
			result = UpdateUser(user)
		}

	case DeleteUser:
		result = "0"
		var token string
		err := json.Unmarshal(payload, &token)
		if nil == err {
			result = DeleteUserAccount(token)
		}

	default:
		log.Printf("418: Unknown command code: %d", command.Type)
		w.WriteHeader(http.StatusTeapot)
		result = fmt.Sprintf("418 - I'm a teapot.\n\nTeapots can't recognize command code %d", command.Type)
	}
	fmt.Fprintf(w, result)
}

func index(w http.ResponseWriter, req *http.Request) {
	if req.URL.Path == "/confirm" {
		confirmUser(w, req) //confirm user registration
	} else if req.URL.Path == "/api" {
		serveApi(w, req) //serve frontend in separate goroutine
	} else if req.URL.Path != "/" {
		// all calls to unknown url paths should return 404
		// log.Printf("404: %s", req.URL.String())
		// http.NotFound(w, req)
		log.Print("Access to: " + req.URL.Path)
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.ServeFile(w, req, GetStaticRoot()+req.URL.Path)
		//return
	} else {
		//fmt.Fprintf(w, "Hi there!") //serve frontend
		http.ServeFile(w, req, GetStaticRoot()+"index.html")
	}
}

func serveHTTP() {
	// redirect every http request to https
	//go http.ListenAndServe(":80", http.HandlerFunc(redirect))
	// serve index (and anything else) as https
	mux := http.NewServeMux()
	mux.HandleFunc("/", index)
	// http.ListenAndServeTLS(":"+GetHTTPSPort, "cert.pem", "key.pem", mux)
	http.ListenAndServe(":"+GetHTTPPort(), mux)
}

func main() {
	InitDb()
	cntxt := &daemon.Context{
		PidFileName: "pid",
		PidFilePerm: 0644,
		LogFileName: "log",
		LogFilePerm: 0640,
		WorkDir:     "./",
		Umask:       027,
		Args:        []string{"[rpicam]"},
	}

	d, err := cntxt.Reborn()
	if err != nil {
		log.Fatal("Unable to run: ", err)
	}
	if d != nil {
		return
	}
	defer cntxt.Release()

	log.Print("- - - - - - - - - - - - - - -")
	log.Print("daemon started")

	serveHTTP()
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func StartVideoStream() string {
	if !isRunning {
		isRunning = true

		raspividArgs := GetRaspividArgs()

		ffmpegArgs := GetFfmpegArgs()

		// commands
		raspivid = exec.Command("raspivid", raspividArgs...)
		psips = exec.Command("psips")
		ffmpeg = exec.Command("ffmpeg", ffmpegArgs...)

		// pipe raspivid's STDOUT to ffmpeg's STDIN
		reader1, writer1 := io.Pipe()
		reader2, writer2 := io.Pipe()

		raspivid.Stdout = writer1
		psips.Stdin = reader1
		psips.Stdout = writer2
		ffmpeg.Stdin = reader2

		// run raspivid
		go func() {
			defer writer1.Close()

			if err := raspivid.Start(); err != nil {
				log.Printf("Error starting raspivid: %s", err.Error())
			}
			log.Printf("started raspivid with args: %s", strings.Join(raspividArgs, " "))

			if err := raspivid.Wait(); err != nil {
				log.Printf("raspivid finished with error: %s", err)
				return
			}
		}()

		// run psips
		go func() {
			defer writer2.Close()

			if err := psips.Start(); err != nil {
				log.Printf("Error starting psips: %s", err.Error())
			}
			log.Printf("started psips")

			if err := psips.Wait(); err != nil {
				log.Printf("psips finished with error: %s", err)
				return
			}
		}()

		// run ffmpeg
		go func() {

			if err := ffmpeg.Start(); err != nil {
				log.Printf("Error starting ffmpeg: %s", err.Error())
			}
			log.Printf("started ffmpeg with args: %s", strings.Join(ffmpegArgs, " "))

			if err := ffmpeg.Wait(); err != nil {
				log.Printf("ffmpeg finished with error: %s", err)
				return
			}
		}()

		//Wait until playlist created
		for {
			_, err := os.Stat("/var/www/html/stream.m3u8")
			if err != nil {
				time.Sleep(5 * time.Millisecond)
			} else {
				return "success"
			}
		}
	} else {
		return "success"
	}
}

func StopVideoStream() string {
	isRunning = false
	if err := raspivid.Process.Signal(syscall.SIGINT); err != nil {
		return fmt.Sprintf("fail: Error sending signal to raspivid: %s", err)
	}

	if err := psips.Process.Signal(syscall.SIGINT); err != nil {
		return fmt.Sprintf("fail: Error sending signal to psips: %s", err)

	}

	if err := ffmpeg.Process.Signal(syscall.SIGINT); err != nil {
		return fmt.Sprintf("fail: Error sending signal to ffmpeg: %s", err)
	}

	//Remove old chunk files and playlist
	files, err := filepath.Glob("/var/www/html/stream*")
	if err != nil {
		return fmt.Sprintf("fail: Error listing .ts files: %s", err)
	}
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			return fmt.Sprintf("fail: Error deleting .ts files: %s", err)
		}
	}

	return "success"
}

func bytestostring(data []byte) string {
	return string(data[:])
}
