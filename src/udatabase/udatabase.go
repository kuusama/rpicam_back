package udatabase

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"log"

	. "../classes"
	"github.com/cznic/ql"
)

func setupDb(db *ql.DB) error {
	ctx := ql.NewRWCtx()
	_, _, err := db.Run(ctx,
		`
        BEGIN TRANSACTION;
        CREATE TABLE IF NOT EXISTS users (
          login STRING
          ,email STRING
		  ,password STRING
		  ,active INT
		  ,token STRING
		  ,level INT
		);
		CREATE INDEX primary_idx_users ON users (id());
        INSERT INTO users (login, email, password, active, token, level) values ("root", "root@mail.server", "0a989ebc4a77b56a6e2bb7b19d995d185ce44090c13e2984b7ecc6d446d4b61ea9991b76a4c2f04b1b4d244841449454", 1, "ea9991b76a4c2f04b1b4d244841449454", 1 );
        COMMIT;
        `)

	if err != nil {
		return err
	}
	return nil
}

func checkDb(db *ql.DB) bool {
	dbinfo, err := db.Info()
	if nil == err {
		for _, table := range dbinfo.Tables {
			if "users" == table.Name {
				return true
			}
		}
	}
	return false
}

func InitDb() {
	ql.RegisterDriver()
	db, err := ql.OpenFile("rpicam.db", &ql.Options{CanCreate: true})
	defer db.Close()
	if err != nil {
		log.Fatalf("failed to open db: %s", err)
	}

	if !checkDb(db) {
		if err = setupDb(db); err != nil {
			log.Fatalf("failed to init database: %s", err)
		}
	}
	db.Close()
}

func connect() *sql.DB {
	ql.RegisterDriver()
	db, err := sql.Open("ql", "./rpicam.db")
	checkErr(err)
	return db
}

func getMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func RegisterUser(user User) (string, int) {
	user.Token = getMD5Hash(user.Email)
	uid := int64(0)

	db := connect()
	result, err := ExecuteQuery(db,
		"INSERT INTO users(login, email, password, active, token, level) VALUES (?1, ?2, ?3, ?4, ?5, ?6);",
		user.Login, user.Email, user.Password, 0, user.Token, 0)

	uid, err = result.LastInsertId()
	checkErr(err)

	db.Close()

	return user.Token, int(uid)
}

func AccountNotActivated(email string) bool {
	count := 0
	db := connect()
	defer db.Close()

	err := db.QueryRow("SELECT count(email) FROM users WHERE email = ?1 AND active = 0", email).Scan(&count)
	checkErr(err)

	return (count > 0)
}

func ExecuteQuery(db *sql.DB, query string, args ...interface{}) (sql.Result, error) {
	var result sql.Result = nil
	var err error = nil
	var stmt *sql.Stmt

	tx, err := db.Begin()

	if err != nil {
		tx.Rollback()
		log.Printf("Error starting transaction! \n\n %s", err)
		goto end
	}

	stmt, err = tx.Prepare(query)
	if err != nil {
		tx.Rollback()
		log.Printf("Error preparing query!\n\n %s", err)
		goto end
	}

	result, err = stmt.Exec(args...)
	if err != nil {
		tx.Rollback()
		log.Printf("Error executing query!\n\n %s", err)
		goto end
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Printf("Error commiting transaction! \n\n %s", err)
		goto end
	}

end:
	return result, err
}

func ActivateUser(token string) bool {
	db := connect()

	result, err := ExecuteQuery(db, "UPDATE users SET active = 1 where token=?1", token)
	rowsAffected, err := result.RowsAffected()

	if err != nil {
		log.Printf("Error getting result! \n\n %s", err)
		return false
	}

	db.Close()

	return (rowsAffected > 0)
}

func CheckEmailRegistred(email string) bool {
	count := 0
	db := connect()
	defer db.Close()

	err := db.QueryRow("SELECT count(email) FROM users WHERE email = $1", email).Scan(&count)
	checkErr(err)

	return (count > 0)
}

func LoginUser(user User) string {
	uid := "0"
	db := connect()
	defer db.Close()
	rows, err := db.Query(
		"SELECT token FROM users WHERE login = $1 and password = $2",
		user.Login, user.Password)

	checkErr(err)

	if rows.Next() {
		rows.Scan(&uid)
	}

	return uid
}

func GetUserList() []User {
	var users []User
	db := connect()
	defer db.Close()
	rows, err := db.Query("SELECT id() as Id, login, email, active, level, token FROM users")
	checkErr(err)
	for rows.Next() {
		var user User
		if err := rows.Scan(&user.Id, &user.Login, &user.Email, &user.Active, &user.Level, &user.Token); err != nil {
			checkErr(err)
		} else {
			users = append(users, user)
		}
	}
	return users
}

func GetFullUserData(user User) User {
	db := connect()
	defer db.Close()
	row := db.QueryRow("SELECT id() as Id, login, email, active, token, level FROM users")
	err := row.Scan(&user.Id, &user.Login, &user.Email, &user.Active,
		&user.Token, &user.Level)
	checkErr(err)
	return user
}

func UpdateUser(user User) string {
	result := "0"

	db := connect()
	if _, err := ExecuteQuery(db,
		"UPDATE users SET login = ?1, email = ?2, password = ?3, active = ?4, level = ?5 WHERE token = ?6;",
		user.Login, user.Email, user.Password, user.Active, user.Level, user.Token); err == nil {
		result = "1"
	}

	db.Close()

	return result
}

func DeleteUserAccount(token string) string {
	result := "0"

	db := connect()
	if _, err := ExecuteQuery(db,
		"DELETE FROM users WHERE token = ?1;", token); err == nil {
		result = "1"
	}

	db.Close()

	return result
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
